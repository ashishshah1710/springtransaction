package com.SpringTransaction.SpringTransaction.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class Patient
{
	 @GeneratedValue(strategy=GenerationType.AUTO)
		private Long patientNumber;
		public Long getPatientNumber() {
		return patientNumber;
	}
	public void setPatientNumber(Long patientNumber) {
		this.patientNumber = patientNumber;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
		public Patient(Long patientNumber, String name, String gender, String mobile, int age) {
		super();
		this.patientNumber = patientNumber;
		Name = name;
		this.gender = gender;
		this.mobile = mobile;
		this.age = age;
	}
		public Patient() {
		super();
		// TODO Auto-generated constructor stub
	}
		private String Name;
		private String gender;
		private String mobile;
		private int age;
}
