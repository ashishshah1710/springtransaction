package com.SpringTransaction.SpringTransaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SpringTransaction.SpringTransaction.controller.dto.AppointmentFormDto;
import com.SpringTransaction.SpringTransaction.service.BookAppointmentRepo;


@RestController
public class BookAppointmentController
{

		@Autowired
		private BookAppointmentRepo bookAppointmentService ;
		
		@PostMapping("/book")
		public String bookAppointment(@RequestBody AppointmentFormDto appointmentFormDto )
		{
			return  bookAppointmentService.bookAppointment(appointmentFormDto);
		}
}
