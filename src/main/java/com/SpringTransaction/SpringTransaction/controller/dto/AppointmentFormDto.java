package com.SpringTransaction.SpringTransaction.controller.dto;

public class AppointmentFormDto
{

	private String Name;
	private String gender;
	private String mobile;
	private int age;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public AppointmentFormDto(String name, String gender, String mobile, int age) {
		super();
		Name = name;
		this.gender = gender;
		this.mobile = mobile;
		this.age = age;
	}
	public AppointmentFormDto() {
		super();
		// TODO Auto-generated constructor stub
	}
}
