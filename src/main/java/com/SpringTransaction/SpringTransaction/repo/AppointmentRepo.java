package com.SpringTransaction.SpringTransaction.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SpringTransaction.SpringTransaction.entity.Appointment;

public interface AppointmentRepo extends JpaRepository<Appointment,Long> {

}
