package com.SpringTransaction.SpringTransaction.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.SpringTransaction.SpringTransaction.entity.Patient;

public interface PatientRepo extends JpaRepository<Patient,Long>{

}
