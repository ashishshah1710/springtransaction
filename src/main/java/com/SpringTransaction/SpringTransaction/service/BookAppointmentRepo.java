package com.SpringTransaction.SpringTransaction.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SpringTransaction.SpringTransaction.controller.dto.AppointmentFormDto;
import com.SpringTransaction.SpringTransaction.entity.Appointment;
import com.SpringTransaction.SpringTransaction.entity.Patient;
import com.SpringTransaction.SpringTransaction.repo.AppointmentRepo;
import com.SpringTransaction.SpringTransaction.repo.PatientRepo;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class BookAppointmentRepo
{
	@Autowired
	private AppointmentRepo appointmentRepo ;

	@Autowired
	private PatientRepo patientRepo  ;
	
	public  String bookAppointment (AppointmentFormDto appointmentFormDto)
	{
		Patient patient = new ObjectMapper().convertValue(appointmentFormDto ,Patient.class);
		Long patientNumber =patientRepo.save(patient).getPatientNumber();
		
		System.out.println("Patient Saved sucessfully");
		Appointment appointment =new Appointment(new Date(System.currentTimeMillis()),101L,patientNumber);
		
		Long appointmentNumber =appointmentRepo.save(appointment).getAppointmentNumber();
		return "Appointment Confirmed" + appointmentNumber;
		
	}

}
